# at_tools

[AT Tools](https://www.drupal.org/project/at_tools) provides theme and admin features that may be required by your theme, required for Adaptivetheme since Drupal 8.

# Adaptivetheme uninstall feature
```sh
composer require drupal/at_tools
drupal module:install at_tools
composer require drupal/pixture_reloaded
drupal theme:install pixture_reloaded

# use it for some time

drupal theme:uninstall pixture_reloaded
# install at_core theme to be able to uninstall it!
drupal theme:install at_core
drupal theme:uninstall at_core
# check site (/admin/reports/status#error /admin/appearance and /admin/reports/updates)
drush pm:list --package adaptivetheme
drupal module:uninstall at_tools
composer remove drupal/pixture_reloaded
composer remove drupal/at_tools
```